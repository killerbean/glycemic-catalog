﻿using GlycemicCatalog.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.Foods.Commands.CreateFood
{
    public class CreateFoodCommandValidator : AbstractValidator<CreateFoodCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateFoodCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Food.Name)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(1024).WithMessage("Title must not exceed 1024 characters.")
                .MustAsync(BeUniqueTitle).WithMessage("The specified title already exists.");
        }

        private async Task<bool> BeUniqueTitle(string name, CancellationToken cancellationToken)
        {
            return await _context.Foods
                .AllAsync(l => l.Name != name, cancellationToken);
        }
    }
}