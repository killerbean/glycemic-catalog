﻿using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using GlycemicCatalog.Application.Foods.Queries.GetFoods;

namespace GlycemicCatalog.Application.Foods.Commands.CreateFood
{
    public class CreateFoodCommand : IRequest<int>
    {
        public FoodDto Food { get; set; }

        public class CreateTodoListCommandHandler : IRequestHandler<CreateFoodCommand, int>
        {
            private readonly IApplicationDbContext _context;

            public CreateTodoListCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(CreateFoodCommand request, CancellationToken cancellationToken)
            {
                var entity = new Food
                {
                    Name = request.Food.Name,
                    Cho = request.Food.Cho,
                    Gi = request.Food.Gi,
                    Gl = request.Food.Gl,
                    GiConfidanceLevel = request.Food.GiConfidanceLevel,
                    Comment = request.Food.Comment
                };

                _context.Foods.Add(entity);
                await _context.SaveChangesAsync(cancellationToken);
                return entity.Id;
            }
        }
    }
}