﻿using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.Foods.Commands.UpdateFood
{
    public class UpdateFoodCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Cho { get; set; }
        public decimal? Gi { get; set; }
        public decimal? Gl { get; set; }
        public decimal? GiConfidanceLevel { get; set; }
        public string Comment { get; set; }

        public class UpdateFoodCommandHandler : IRequestHandler<UpdateFoodCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateFoodCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateFoodCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Foods.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Food), request.Id);
                }

                entity.Name = request.Name;
                entity.Cho = request.Cho;
                entity.Gi = request.Gi;
                entity.Gl = request.Gl;
                entity.GiConfidanceLevel = request.GiConfidanceLevel;
                entity.Comment = request.Comment;

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}