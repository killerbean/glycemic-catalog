﻿using GlycemicCatalog.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.Foods.Commands.UpdateFood
{
    public class UpdateFoodCommandValidator : AbstractValidator<UpdateFoodCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateFoodCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Name)
                .NotEmpty().WithMessage("Title is required.")
                .MaximumLength(1024).WithMessage("Title must not exceed 1024 characters.")
                .MustAsync(BeUniqueTitle).WithMessage("The specified title is already exists.");
        }

        private async Task<bool> BeUniqueTitle(UpdateFoodCommand model, string name, CancellationToken cancellationToken)
        {
            return await _context.Foods
                .Where(l => l.Id != model.Id)
                .AllAsync(l => l.Name != name, cancellationToken);
        }
    }
}
