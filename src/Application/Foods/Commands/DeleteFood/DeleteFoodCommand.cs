﻿using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.Foods.Commands.DeleteFood
{
    public class DeleteFoodCommand : IRequest
    {
        public int Id { get; set; }

        public class DeleteFoodCommandHandler : IRequestHandler<DeleteFoodCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteFoodCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteFoodCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Foods
                    .Where(l => l.Id == request.Id)
                    .SingleOrDefaultAsync(cancellationToken);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Food), request.Id);
                }

                _context.Foods
                    .Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
