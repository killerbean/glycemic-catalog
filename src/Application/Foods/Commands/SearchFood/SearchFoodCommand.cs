﻿using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Common;
using GlycemicCatalog.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace GlycemicCatalog.Application.Foods.Commands.SearchFood
{
    public class SearchFoodCommand : IRequest
    {
        public int Id { get; set; }
        
        public class SearchFoodCommandHandler : IRequestHandler<SearchFoodCommand>
        {
            private readonly IApplicationDbContext _context;
            private readonly SearchEngine _searchEngine;

            public SearchFoodCommandHandler(IApplicationDbContext context, IOptions<SearchEngine> searchEngine)
            {
                _context = context;
                _searchEngine = searchEngine.Value;
            }
            
            public async Task<Unit> Handle(SearchFoodCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Foods
                    .Where(l => l.Id == request.Id)
                    .SingleOrDefaultAsync(cancellationToken);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Food), request.Id);
                }

                var response = await SearchImage(entity.Name);

                foreach (var img in response.value)
                {
                    var foodImage = new FoodImage
                    {
                        FoodId = request.Id,
                        FoodForeignKey = request.Id,
                        Name = img.name,
                        ContentUrl = img.contentUrl,
                        ThumbnailUrl = img.thumbnailUrl,
                        HostpageUrl = img.hostPageUrl
                    };

                    _context.FoodImages.Add(foodImage);

                    await _context.SaveChangesAsync(cancellationToken);
                }


                return Unit.Value;
            }
            
            private async Task<BingImageSearchResponse> SearchImage(string searchTerm)  
            {  
                var client = new HttpClient();  
                var queryString = HttpUtility.ParseQueryString(string.Empty);  
   
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", _searchEngine.ApiKey);
                queryString["q"] = searchTerm;  
                queryString["count"] = "10";
                queryString["mkt"] = "en-us";
                var uri = _searchEngine.Url + "?" + queryString;  
  
                var response = await client.GetAsync(uri);  
                var json = await response.Content.ReadAsStringAsync();
                return   JsonConvert.DeserializeObject<BingImageSearchResponse>(json);
            } 
        }
        
    }
}