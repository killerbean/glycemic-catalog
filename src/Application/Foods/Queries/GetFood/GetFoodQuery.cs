﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Application.Foods.Queries.GetFoods;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace GlycemicCatalog.Application.Foods.Queries.GetFood
{
    public class GetFoodQuery: IRequest<FoodsVm>
    {
        public long Id { get; set; }
        
        public class GetFoodQueryHandler : IRequestHandler<GetFoodQuery, FoodsVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetFoodQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<FoodsVm> Handle(GetFoodQuery request, CancellationToken cancellationToken)
            {
                var vm = new FoodsVm
                {
                    Lists = await _context.Foods
                        .ProjectTo<FoodDto>(_mapper.ConfigurationProvider)
                        .Where(f => f.Id == request.Id)
                        .OrderBy(t => t.Name)
                        .ToListAsync(cancellationToken)
                };

                vm.Lists[0].Items = await _context.FoodImages
                    .ProjectTo<FoodImageDto>(_mapper.ConfigurationProvider)
                    .Where(fi => fi.FoodId == request.Id)
                    .OrderBy(fi => fi.Name)
                    .ToListAsync(cancellationToken);
                
                return vm;
            }
        }
    }
}
