﻿using AutoMapper;
using GlycemicCatalog.Application.Common.Mappings;
using GlycemicCatalog.Domain.Entities;

namespace GlycemicCatalog.Application.Foods.Queries.GetFoods
{
    public class FoodImageDto : IMapFrom<FoodImage>
    {
        public int Id { get; set; }
        public int FoodId { get; set; }
        public string Name { get; set; }
        public string ContentUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string HostpageUrl { get; set; }

        public int FoodForeignKey { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap(typeof(FoodImage), GetType());
        }
    }
}
