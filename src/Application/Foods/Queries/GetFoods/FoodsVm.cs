﻿using System.Collections.Generic;

namespace GlycemicCatalog.Application.Foods.Queries.GetFoods
{
    public class FoodsVm
    {
        public IList<FoodDto> Lists { get; set; }
    }
}
