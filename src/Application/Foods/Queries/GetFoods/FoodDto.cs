﻿using System.Collections.Generic;
using AutoMapper;
using GlycemicCatalog.Application.Common.Mappings;
using GlycemicCatalog.Domain.Entities;

namespace GlycemicCatalog.Application.Foods.Queries.GetFoods
{
    public class FoodDto: IMapFrom<Food>
    {
        public FoodDto()
        {
            Items = new List<FoodImageDto>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Cho { get; set; }
        public decimal? Gi { get; set; }
        public decimal? Gl { get; set; }
        public decimal? GiConfidanceLevel { get; set; }
        public string Comment { get; set; }

        public IList<FoodImageDto> Items { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap(typeof(Food), GetType());
        }
    }
}