﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using GlycemicCatalog.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GlycemicCatalog.Domain.Entities;

namespace GlycemicCatalog.Application.Foods.Queries.GetFoods
{
    public class GetFoodsQuery: IRequest<FoodsVm>
    {
        public class GetFoodsQueryHandler : IRequestHandler<GetFoodsQuery, FoodsVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetFoodsQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<FoodsVm> Handle(GetFoodsQuery request, CancellationToken cancellationToken)
            {
                var vm = new FoodsVm
                {
                    Lists = await _context.Foods
                        .ProjectTo<FoodDto>(_mapper.ConfigurationProvider)
                        .OrderBy(t => t.Name)
                        .ToListAsync(cancellationToken)
                };
                
                foreach (var foodDto in vm.Lists)
                {
                    foodDto.Items = await _context.FoodImages
                        .ProjectTo<FoodImageDto>(_mapper.ConfigurationProvider)
                        .Where(fi => fi.FoodId == foodDto.Id)
                        .OrderBy(fi => fi.Name)
                        .ToListAsync(cancellationToken);

                    if (foodDto.Items.Any()) 
                        continue;
                    
                    // Random placeholder from internet
                    const string notAvailableUrl = "https://image.shutterstock.com/image-vector/photo-coming-soon-symbol-600w-342504203.jpg";
                    var notAvailableItem = new FoodImageDto
                    {
                        Id = -1,
                        FoodId = foodDto.Id,
                        FoodForeignKey = foodDto.Id,
                        Name = foodDto.Name,
                        ContentUrl = notAvailableUrl,
                        HostpageUrl = notAvailableUrl,
                        ThumbnailUrl = notAvailableUrl
                    };
                    
                    foodDto.Items.Add(notAvailableItem);
                }

                return vm;
            }
        }
    }
}