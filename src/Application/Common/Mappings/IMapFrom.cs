﻿using AutoMapper;

namespace GlycemicCatalog.Application.Common.Mappings
{
    public interface IMapFrom<T>
    {   
        void Mapping(Profile profile);
    }
}
