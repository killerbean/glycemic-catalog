﻿using GlycemicCatalog.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Food> Foods { get; set; }

        DbSet<FoodImage> FoodImages { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
