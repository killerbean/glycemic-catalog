﻿using System;

namespace GlycemicCatalog.Application.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
