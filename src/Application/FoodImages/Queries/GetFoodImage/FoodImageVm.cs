﻿using System.Collections.Generic;
using GlycemicCatalog.Application.Foods.Queries.GetFoods;

namespace GlycemicCatalog.Application.FoodImages.Queries.GetFoodImage
{
    public class FoodImageVm
    {
        public IList<FoodImageDto> Result { get; set; }
        public string Message { get; set; }
    }
}
