﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Application.Foods.Queries.GetFoods;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace GlycemicCatalog.Application.FoodImages.Queries.GetFoodImage
{
    public class GetFoodImagesQuery : IRequest<FoodImageVm>
    {
        public long Id { get; set; }

        public class GetFoodsQueryHandler : IRequestHandler<GetFoodImagesQuery, FoodImageVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetFoodsQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<FoodImageVm> Handle(GetFoodImagesQuery request, CancellationToken cancellationToken)
            {
                var result = await _context.FoodImages
                    .ProjectTo<FoodImageDto>(_mapper.ConfigurationProvider)
                    .Where(fi => fi.FoodId == request.Id)
                    .ToListAsync(cancellationToken);

                return new FoodImageVm
                {
                    Message = result == null ? "food not found" : "success",
                    Result = result
                };
            }
        }
    }
}
