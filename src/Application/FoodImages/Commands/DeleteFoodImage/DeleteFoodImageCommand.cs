﻿using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.FoodImages.Commands.DeleteFoodImage
{
    public class DeleteFoodImageCommand : IRequest
    {
        public long Id { get; set; }

        public class DeleteTodoItemCommandHandler : IRequestHandler<DeleteFoodImageCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteTodoItemCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteFoodImageCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.FoodImages.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(FoodImage), request.Id);
                }

                _context.FoodImages.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
