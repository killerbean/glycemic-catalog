﻿using FluentValidation;

namespace GlycemicCatalog.Application.FoodImages.Commands.UpdateFoodImage
{
    public class UpdateFoodImageCommandValidator : AbstractValidator<UpdateFoodImageCommand>
    {
        public UpdateFoodImageCommandValidator()
        {
            RuleFor(v => v.Name)
                .MaximumLength(1024)
                .NotEmpty();
        }
    }
}
