﻿using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.FoodImages.Commands.UpdateFoodImage
{
    public class UpdateFoodImageCommand : IRequest
    {
        public long Id { get; set; }
        public int FoodId { get; set; }
        public string Name { get; set; }
        public string ContentUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string HostpageUrl { get; set; }

        public class UpdateFoodImageCommandHandler : IRequestHandler<UpdateFoodImageCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateFoodImageCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateFoodImageCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.FoodImages.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(FoodImage), request.Id);
                }

                entity.FoodId = request.FoodId;
                entity.Name = request.Name;
                entity.ContentUrl = request.ContentUrl;
                entity.ThumbnailUrl = request.ThumbnailUrl;
                entity.HostpageUrl = request.HostpageUrl;

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
