﻿using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GlycemicCatalog.Application.FoodImages.Commands.CreateFoodImage
{
    public class CreateFoodImageCommand : IRequest<long>
    {
        public int FoodId { get; set; }
        public string Name { get; set; }
        public string ContentUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string HostpageUrl { get; set; }

        public class CreateTodoItemCommandHandler : IRequestHandler<CreateFoodImageCommand, long>
        {
            private readonly IApplicationDbContext _context;

            public CreateTodoItemCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<long> Handle(CreateFoodImageCommand request, CancellationToken cancellationToken)
            {
                var entity = new FoodImage
                {
                    FoodId = request.FoodId,
                    Name = request.Name,
                    ContentUrl = request.ContentUrl,
                    ThumbnailUrl = request.ThumbnailUrl,
                    HostpageUrl = request.HostpageUrl
                };

                _context.FoodImages.Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return entity.Id;
            }
        }
    }
}
