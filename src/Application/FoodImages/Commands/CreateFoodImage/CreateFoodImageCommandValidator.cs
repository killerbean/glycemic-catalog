﻿using FluentValidation;

namespace GlycemicCatalog.Application.FoodImages.Commands.CreateFoodImage
{
    public class CreateFoodImageCommandValidator : AbstractValidator<CreateFoodImageCommand>
    {
        public CreateFoodImageCommandValidator()
        {
            RuleFor(v => v.Name)
                .MaximumLength(1024)
                .NotEmpty();
        }
    }
}
