﻿using System.Collections.Generic;
using GlycemicCatalog.Domain.Common;

namespace GlycemicCatalog.Domain.Entities
{
    public class Food: AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Cho { get; set; }
        public decimal? Gi { get; set; }
        public decimal? Gl { get; set; }
        public decimal? GiConfidanceLevel { get; set; }
        public string Comment { get; set; }

        public List<FoodImage> FoodImages { get; set; }
    }
}
