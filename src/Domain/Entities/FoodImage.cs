﻿using GlycemicCatalog.Domain.Common;

namespace GlycemicCatalog.Domain.Entities
{
    public class FoodImage: AuditableEntity
    {
        public int Id { get; set; }
        public int FoodId { get; set; }
        public string Name { get; set; }
        public string ContentUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string HostpageUrl { get; set; }

        public int FoodForeignKey { get; set; }
        public Food Food { get; set; }
    }
}
