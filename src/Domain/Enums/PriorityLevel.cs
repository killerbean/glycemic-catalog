﻿namespace GlycemicCatalog.Domain.Enums
{
    public enum PriorityLevel
    {
        None,
        Low,
        Medium,
        High
    }
}
