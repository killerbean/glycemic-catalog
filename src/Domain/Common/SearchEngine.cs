﻿namespace GlycemicCatalog.Domain.Common
{
    public class SearchEngine
    {
        public string Url { get; set; }
        public string ApiKey { get; set; }
    }
}
