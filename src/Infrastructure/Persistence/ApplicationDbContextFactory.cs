﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GlycemicCatalog.Infrastructure.Persistence
{
    public class ApplicationDbContextFactory: IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>();
            var config = GetAppConfiguration();
            options.UseSqlServer(config.GetConnectionString("DefaultConnection"));

            // return new ApplicationDbContext(options.Options);
            return new ApplicationDbContext(options.Options, null, null, null);
        }
        
        IConfiguration GetAppConfiguration()
        {
            var environmentName =
                Environment.GetEnvironmentVariable(
                    "ASPNETCORE_ENVIRONMENT");

            var dir = Directory.GetParent(AppContext.BaseDirectory);    
            do
                dir = dir.Parent;
            while (dir.Name != "bin");
            dir = dir.Parent;
            var path = dir.Parent + "\\WebUI";
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(path)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{environmentName}.json", true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
