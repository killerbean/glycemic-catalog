﻿using GlycemicCatalog.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GlycemicCatalog.Infrastructure.Persistence.Configurations
{
    public class FoodImageConfiguration
    {
        public void Configure(EntityTypeBuilder<FoodImage> builder)
        {
            builder.ToTable("TBL_FOODS", "food");
            builder.HasKey(f => f.Id);
            builder.Property(c => c.Id).HasColumnName("FI_ID");
            builder.Property(c => c.FoodId).HasColumnName("FI_F_ID");
            builder.Property(c => c.Name).HasColumnName("FI_IMAGE_NAME").HasMaxLength(1024);
            builder.Property(c => c.ContentUrl).HasColumnName("FI_CONTENT_URL").HasMaxLength(1024);
            builder.Property(c => c.ThumbnailUrl).HasColumnName("FI_THUMBNAIL_URL").HasMaxLength(1024);
            builder.Property(c => c.HostpageUrl).HasColumnName("FI_HOSTPAGE_URL").HasMaxLength(1024);
            
            builder.Property(c => c.CreatedBy).HasColumnName("FI_CREATED_BY").HasMaxLength(1024);
            builder.Property(c => c.Created).HasColumnName("FI_CREATED");
            builder.Property(c => c.LastModifiedBy).HasColumnName("FI_LAST_MODIFIED_BY").HasMaxLength(1024);
            builder.Property(c => c.LastModified).HasColumnName("FI_LAST_MODIFIED");
            
            builder.HasOne(f => f.Food)
                .WithMany(f => f.FoodImages)
                .HasForeignKey(f => f.FoodForeignKey);
        }
    }
}