﻿using GlycemicCatalog.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GlycemicCatalog.Infrastructure.Persistence.Configurations
{
    public class FoodConfiguration
    {
        public void Configure(EntityTypeBuilder<Food> builder)
        {
            builder.ToTable("TBL_FOODS", "food");
            builder.HasKey(f => f.Id);
            builder.Property(c => c.Id).HasColumnName("F_ID");
            builder.Property(c => c.Name).HasColumnName("F_NAME").HasMaxLength(1024);
            builder.Property(c => c.Cho).HasColumnName("F_CHO").HasColumnType("decimal(18,0)");
            builder.Property(c => c.Gi).HasColumnName("F_GI").HasColumnType("decimal(18,0)");
            builder.Property(c => c.Gl).HasColumnName("F_GL").HasColumnType("decimal(18,0)");
            builder.Property(c => c.GiConfidanceLevel).HasColumnName("F_GI_CONFIDANCE_LEVEL").HasColumnType("decimal(18,0)");
            builder.Property(c => c.Comment).HasColumnName("F_COMMENT").HasMaxLength(1024);
            builder.Property(c => c.CreatedBy).HasColumnName("F_CREATED_BY").HasMaxLength(1024);
            builder.Property(c => c.Created).HasColumnName("F_CREATED");
            builder.Property(c => c.LastModifiedBy).HasColumnName("F_LAST_MODIFIED_BY").HasMaxLength(1024);
            builder.Property(c => c.LastModified).HasColumnName("F_LAST_MODIFIED");
        }
    }
}