﻿
using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GlycemicCatalog.Infrastructure.Persistence.Migrations
{
    public partial class FoodsMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.DropTable(
//                name: "AspNetRoleClaims");
//
//            migrationBuilder.DropTable(
//                name: "AspNetUserClaims");
//
//            migrationBuilder.DropTable(
//                name: "AspNetUserLogins");
//
//            migrationBuilder.DropTable(
//                name: "AspNetUserRoles");
//
//            migrationBuilder.DropTable(
//                name: "AspNetUserTokens");
//
//            migrationBuilder.DropTable(
//                name: "DeviceCodes");
//
//            migrationBuilder.DropTable(
//                name: "PersistedGrants");
//            
//            migrationBuilder.DropTable(
//                name: "AspNetRoles");
//
//            migrationBuilder.DropTable(
//                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Foods");
            
            migrationBuilder.DropTable(
                name: "FoodImages");

            migrationBuilder.CreateTable(
                name: "Foods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Cho = table.Column<decimal>(nullable: true),
                    Gi = table.Column<decimal>(nullable: true),
                    Gl = table.Column<decimal>(nullable: true),
                    GiConfidanceLevel = table.Column<decimal>(nullable: true),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Foods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FoodImages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    FoodId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ContentUrl = table.Column<string>(nullable: true),
                    ThumbnailUrl = table.Column<string>(nullable: true),
                    HostpageUrl = table.Column<string>(nullable: true),
                    FoodForeignKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FoodImages_Foods_FoodId",
                        column: x => x.FoodId,
                        principalTable: "Foods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FoodImages_FoodId",
                table: "FoodImages",
                column: "FoodId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FoodImages");

            migrationBuilder.DropTable(
                name: "Foods");
        }
    }
}
