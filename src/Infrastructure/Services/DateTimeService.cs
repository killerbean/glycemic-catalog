﻿using GlycemicCatalog.Application.Common.Interfaces;
using System;

namespace GlycemicCatalog.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
