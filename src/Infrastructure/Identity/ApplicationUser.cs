﻿using Microsoft.AspNetCore.Identity;

namespace GlycemicCatalog.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
