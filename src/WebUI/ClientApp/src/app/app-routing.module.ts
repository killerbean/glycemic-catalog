import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
    DxDataGridModule,
    DxTileViewModule,
    DxButtonModule,
    DxPopupModule,
    DxTextBoxModule
} from 'devextreme-angular';
import { HomeComponent } from './home/home.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { FoodListModule } from './components/food-list/food-list.component';

const routes: Routes = [
    { 
        path: '', 
        component: HomeComponent, 
        pathMatch: 'full' 
    },
    { 
        path: 'fetch-data', 
        component: FetchDataComponent, 
        canActivate: [ AuthorizeGuard ]
    },
    {
      path: 'details/:id',
      loadChildren: () => import('src/app/details/details.module').then(m => m.DetailsModule),
      canActivate: [ AuthorizeGuard ]
    },
];

    @NgModule({
        imports: [
          CommonModule,
          RouterModule.forRoot(routes),          
          DxButtonModule,
          DxDataGridModule,
          DxTileViewModule,
          DxPopupModule,
          DxTextBoxModule,
          FoodListModule
        ],
        providers: [ AuthorizeGuard ],
        exports: [RouterModule],
        declarations: [
          HomeComponent,
          FetchDataComponent
        ]
      })
      export class AppRoutingModule { }
      