import { Component } from '@angular/core';
import {
  FoodsClient,
  FoodDto,
  FoodsVm,
  CreateFoodCommand
} from '../glycemiccatalog-api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  vm: FoodsVm;
  selectedFood: FoodDto;
  popupVisible = false;
  newFood: FoodDto = new FoodDto();

  constructor(
    private foodsClient: FoodsClient
  ) {
    foodsClient.get().subscribe(
      result => {
        this.vm = result;
        if (this.vm.lists.length) {
          this.selectedFood = this.vm.lists[0];
        }
      },
      error => console.error(error)
    );
  }

  addFood() {
    this.popupVisible = true;
  }

  saveFood() {
    this.foodsClient.create(<CreateFoodCommand>{ food: this.newFood })
      .subscribe(() => {
        this.popupVisible = false;
      });
  }
}
