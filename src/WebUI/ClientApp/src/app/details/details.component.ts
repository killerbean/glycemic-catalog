import { Component, OnInit } from '@angular/core';
import { FoodDto, FoodsClient, FoodsVm, FoodImageDto } from '../glycemiccatalog-api';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  food: FoodDto;
  loadingVisible = false;
  foodParameters: any;
  images: string[] = new Array();

  constructor(
    private activatedRoute: ActivatedRoute,
    private foodsClient: FoodsClient,
    private router: Router
  ) { }

  ngOnInit() {
    var foodId = this.activatedRoute.snapshot.params.id;

    this.foodsClient.getById(foodId)
      .subscribe((success: FoodsVm) => {
        this.food = success.lists[0];
        this.foodParameters = this.processFoodParameters(this.food);
        this.processImages(this.food.items);
      });
  }

  processFoodParameters(food: FoodDto) {
    return [
      {
        characteristics: "Cho",
        value: food.cho
      },
      {
        characteristics: "Glycemic index",
        value: food.gi
      },
      {
        characteristics: "Glycemic index confidence",
        value: food.giConfidanceLevel
      },
      {
        characteristics: "Glycemic level",
        value: food.gl
      },
    ]
  }

  processImages(foodImage: FoodImageDto[]) {
    return foodImage.forEach(item => {
      this.images.push(item.contentUrl);
    })
  }

  search(id: number) {
    this.loadingVisible = true;
    this.foodsClient.search(id)
      .subscribe(
        () => {        
          this.foodsClient.getById(id)
            .subscribe((success: FoodsVm) => {
              this.food = success.lists[0];
              this.foodParameters = this.processFoodParameters(this.food);
              this.processImages(this.food.items);
              this.loadingVisible = false;
            });
        },
        error => {
          this.loadingVisible = false;
        }
      );
  }

  goBack() {
    this.router.navigateByUrl('');
  }
}
