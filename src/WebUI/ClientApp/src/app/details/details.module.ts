import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { DetailsComponent } from "./details.component";
import {
    DxDataGridModule,
    DxGalleryModule,
    DxButtonModule,
    DxLoadPanelModule
} from "devextreme-angular";

const routes: Routes = [
    { path: '', component: DetailsComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        DxButtonModule,
        DxDataGridModule,
        DxGalleryModule,
        DxLoadPanelModule,
        RouterModule.forChild(routes),
    ],
    declarations: [DetailsComponent],
    exports: [DetailsComponent]
})
export class DetailsModule { }
