import { Component, OnInit, NgModule, Input } from '@angular/core';
import { FoodItemModule } from '../food-item/food-item.component';
import { CommonModule } from '@angular/common';
import { DxButtonModule } from 'devextreme-angular';
import { FoodsVm, FoodDto } from 'src/app/glycemiccatalog-api';

@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.component.html',
  styleUrls: ['./food-list.component.css']
})
export class FoodListComponent implements OnInit {
  @Input() food: FoodDto;

  constructor() { }

  ngOnInit() {
  }

}

@NgModule({
  imports: [
    CommonModule,
    FoodItemModule,
    DxButtonModule
  ],
  declarations: [
    FoodListComponent
  ],
  exports: [ FoodListComponent ]
})
export class FoodListModule { }
