import { Component, OnInit, NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxButtonModule, DxLoadPanelModule } from 'devextreme-angular';
import { FoodImageDto, FoodDto, FoodsClient, FoodsVm } from 'src/app/glycemiccatalog-api';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-food-item',
  templateUrl: './food-item.component.html',
  styleUrls: ['./food-item.component.css']
})
export class FoodItemComponent implements OnInit {
  @Input() food: FoodDto;
  currentImage: FoodImageDto;
  loadingVisible = false;

  constructor(
    private foodsClient: FoodsClient
  ) { }

  ngOnInit() {
    this.currentImage = this.randimize();
  }

  randimize() {
    var image = new FoodImageDto();
    var canReturn = false;
    var maxLoops = 0;
    do {
      if (this.food.items[this.getRandomIndex()] !== undefined) {
        image = this.food.items[this.getRandomIndex()];
      }

      maxLoops++;

      if (image !== null || maxLoops === 30) {
        canReturn = true;
      }
    } while (!canReturn);

    return image;
  }

  getRandomIndex() {
    return Math.floor(Math.random() * this.food.items.length);
  }

  search(id: number) {
    this.loadingVisible = true;
    this.foodsClient.search(id)
      .subscribe(
        () => {        
          this.foodsClient.getById(id)
            .subscribe((success: FoodsVm) => {
              this.food = success.lists[0];
              this.currentImage = this.randimize();
              this.loadingVisible = false;
            });
        },
        error => {
          this.loadingVisible = false;
        }
      );
  }
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxButtonModule,
    DxLoadPanelModule
  ],
  declarations: [FoodItemComponent],
  exports: [FoodItemComponent]
})
export class FoodItemModule { }
