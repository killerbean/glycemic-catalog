﻿using GlycemicCatalog.Application.FoodImages.Commands.CreateFoodImage;
using GlycemicCatalog.Application.FoodImages.Commands.DeleteFoodImage;
using GlycemicCatalog.Application.FoodImages.Commands.UpdateFoodImage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using GlycemicCatalog.Application.FoodImages.Queries.GetFoodImage;

namespace GlycemicCatalog.WebUI.Controllers
{
    [Authorize]
    public class FoodImagesController : ApiController
    {
        [HttpGet("{id}")]
        public async Task<ActionResult<FoodImageVm>> Get(long id)
        {
            return await Mediator.Send(new GetFoodImagesQuery { Id = id });
        }
        
        [HttpPost]
        public async Task<ActionResult<long>> Create(CreateFoodImageCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(long id, UpdateFoodImageCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteFoodImageCommand { Id = id });

            return NoContent();
        }
    }
}
