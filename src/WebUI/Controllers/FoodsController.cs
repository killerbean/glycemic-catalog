﻿using GlycemicCatalog.Application.Foods.Commands.CreateFood;
using GlycemicCatalog.Application.Foods.Commands.DeleteFood;
using GlycemicCatalog.Application.Foods.Commands.UpdateFood;
using GlycemicCatalog.Application.Foods.Queries.GetFoods;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using GlycemicCatalog.Application.Foods.Commands.SearchFood;
using GlycemicCatalog.Application.Foods.Queries.GetFood;

namespace GlycemicCatalog.WebUI.Controllers
{
    [Authorize]
    public class FoodsController : ApiController

    {
        [HttpGet]
        public async Task<ActionResult<FoodsVm>> Get()
        {
            return await Mediator.Send(new GetFoodsQuery());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<FoodsVm>> GetById(long id)
        {
            return await Mediator.Send(new GetFoodQuery { Id = id });
        }

        [HttpPost]
        public async Task<ActionResult<long>> Create(CreateFoodCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(long id, UpdateFoodCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteFoodCommand { Id = id });

            return NoContent();
        }
        
        [HttpPut("/search/{id}")]
        public async Task<ActionResult> Search(int id)
        {
            await Mediator.Send(new SearchFoodCommand { Id = id });

            return NoContent();
        }
    }
}
