## This application inspired by Clean Architecture Solution Template 
[![Clean.Architecture.Solution.Template NuGet Package](https://img.shields.io/badge/nuget-1.0.4-blue)](https://www.nuget.org/packages/Clean.Architecture.Solution.Template)

## Technologies
* .NET Core 3
* ASP .NET Core 3
* Entity Framework Core 3
* Angular 8

## Few words

I have created this project just for learning purposes, and to demonstrate my software development skills. If you have any questions feel free to ask me in Telegram [@Delphix](https://telegram.me/Delphix).
