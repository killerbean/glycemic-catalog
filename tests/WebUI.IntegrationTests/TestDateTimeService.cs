﻿using GlycemicCatalog.Application.Common.Interfaces;
using System;

namespace GlycemicCatalog.WebUI.IntegrationTests
{
    public class TestDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
