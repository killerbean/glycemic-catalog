﻿using GlycemicCatalog.Application;
using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using GlycemicCatalog.Infrastructure.Persistence;
using IdentityModel.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace GlycemicCatalog.WebUI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder
                .ConfigureServices(services =>
                {
                    // Create a new service provider.
                    var serviceProvider = new ServiceCollection()
                        .AddEntityFrameworkInMemoryDatabase()
                        .BuildServiceProvider();

                    // Add a database context using an in-memory 
                    // database for testing.
                    services.AddDbContext<ApplicationDbContext>(options =>
                    {
                        options.UseInMemoryDatabase("InMemoryDbForTesting");
                        options.UseInternalServiceProvider(serviceProvider);
                    });

                    services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());

                    services.AddScoped<ICurrentUserService, TestCurrentUserService>();
                    services.AddScoped<IDateTime, TestDateTimeService>();
                    services.AddScoped<IIdentityService, TestIdentityService>();

                    var sp = services.BuildServiceProvider();

                    // Create a scope to obtain a reference to the database
                    using var scope = sp.CreateScope();
                    var scopedServices = scope.ServiceProvider;
                    var context = scopedServices.GetRequiredService<ApplicationDbContext>();
                    var logger = scopedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                    // Ensure the database is created.
                    context.Database.EnsureCreated();

                    try
                    {
                        SeedSampleData(context);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"An error occurred seeding the database with sample data. Error: {ex.Message}.");
                    }
                })
                .UseEnvironment("Test");
        }

        public HttpClient GetAnonymousClient()
        {
            return CreateClient();
        }

        public async Task<HttpClient> GetAuthenticatedClientAsync()
        {
            return await GetAuthenticatedClientAsync("jason@clean-architecture", "GlycemicCatalog!");
        }

        public async Task<HttpClient> GetAuthenticatedClientAsync(string userName, string password)
        {
            var client = CreateClient();

            var token = await GetAccessTokenAsync(client, userName, password);

            client.SetBearerToken(token);

            return client;
        }

        private async Task<string> GetAccessTokenAsync(HttpClient client, string userName, string password)
        {
            var disco = await client.GetDiscoveryDocumentAsync();

            if (disco.IsError)
            {
                throw new Exception(disco.Error);
            }

            var response = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "GlycemicCatalog.IntegrationTests",
                ClientSecret = "secret",

                Scope = "GlycemicCatalog.WebUIAPI openid profile",
                UserName = userName,
                Password = password
            });

            if (response.IsError)
            {
                throw new Exception(response.Error);
            }

            return response.AccessToken;
        }

        public static void SeedSampleData(ApplicationDbContext context)
        {
            context.Foods.AddRange(
                new Food
                {
                    Id = 1,
                    Name = "Bran. wheat",
                    Cho = 27,
                    Gi = 70,
                    Gl = 19,
                    GiConfidanceLevel = 5,
                    Comment = ""
                }
            );

            context.FoodImages.AddRange(
                new FoodImage
                {
                    Id = 1,
                    FoodId = 1,
                    Name = "Wheat Bran – Botanical Colors",
                    ContentUrl = "https://botanicalcolors.com/dir/wp-content/uploads/2018/05/Wheat-Bran.png",
                    ThumbnailUrl = "https://tse2.mm.bing.net/th?id=OIP.lu6mHfalTAtf6QDB_NwergHaHa&pid=Api",
                    HostpageUrl = "https://botanicalcolors.com/shop/mordants/wheat-bran/"
                },
                new FoodImage
                {
                    Id = 2,
                    FoodId = 1,
                    Name = "Wheat Bran Vs. Oat Bran | LIVESTRONG.COM",
                    ContentUrl =
                        "http://img.aws.livestrongcdn.com/ls-article-image-673/cpi.studiod.com/www_livestrong_com/photos.demandstudios.com/getty/article/167/179/97030033_XS.jpg",
                    ThumbnailUrl = "https://tse4.mm.bing.net/th?id=OIP.pm-hVdRM1rDizW-PAHGmBAHaE6&pid=Api",
                    HostpageUrl = "http://www.livestrong.com/article/41815-wheat-bran-vs.-oat-bran/"
                },
                new FoodImage
                {
                    Id = 3,
                    FoodId = 1,
                    Name = "Know the Real Difference Between Wheat Germ and Wheat Bran ...",
                    ContentUrl =
                        "http://media.buzzle.com/media/images-en/photos/agriculture/1200-20995082-wheat-bran.jpg",
                    ThumbnailUrl = "https://tse2.mm.bing.net/th?id=OIP.AImTuYsIkoxz03k5_BV-RwHaE8&pid=Api",
                    HostpageUrl =
                        "http://www.buzzle.com/articles/what-is-the-difference-between-wheat-germ-and-wheat-bran.html"
                }
            );

            context.SaveChanges();
        }
    }
}
