﻿using GlycemicCatalog.Application.FoodImages.Commands.UpdateFoodImage;
using System.Threading.Tasks;
using Xunit;

namespace GlycemicCatalog.WebUI.IntegrationTests.Controllers.TodoItems
{
    public class Update : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Update(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenValidUpdateFoodImageCommand_ReturnsSuccessCode()
        {
            var client = await _factory.GetAuthenticatedClientAsync();

            var command = new UpdateFoodImageCommand
            { 
                Id = 1, 
                Name = "Wheat Bran – Botanical Colors"
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PutAsync($"/api/todoitems/{command.Id}", content);

            response.EnsureSuccessStatusCode();
        }
    }
}
