using GlycemicCatalog.Application.Common.Interfaces;
using GlycemicCatalog.Domain.Entities;
using GlycemicCatalog.Infrastructure.Persistence;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using System;

namespace GlycemicCatalog.Application.UnitTests.Common
{
    public static class ApplicationDbContextFactory
    {
        public static ApplicationDbContext Create()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var operationalStoreOptions = Options.Create(
                new OperationalStoreOptions
                {
                    DeviceFlowCodes = new TableConfiguration("DeviceCodes"),
                    PersistedGrants = new TableConfiguration("PersistedGrants")
                });

            var dateTimeMock = new Mock<IDateTime>();
            dateTimeMock.Setup(m => m.Now)
                .Returns(new DateTime(3001, 1, 1));

            var currentUserServiceMock = new Mock<ICurrentUserService>();
            currentUserServiceMock.Setup(m => m.UserId)
                .Returns("00000000-0000-0000-0000-000000000000");

            var context = new ApplicationDbContext(
                options, operationalStoreOptions,
                currentUserServiceMock.Object, dateTimeMock.Object);

            context.Database.EnsureCreated();

            SeedSampleData(context);

            return context;
        }

        public static void SeedSampleData(ApplicationDbContext context)
        {
            context.Foods.AddRange(
                new Food 
                {
                    Id = 1, 
                    Name = "Bran. wheat",
                    Cho = 27,
                    Gi = 70,
                    Gl = 19,
                    GiConfidanceLevel = 5,
                    Comment = ""
                }
            );

            context.FoodImages.AddRange(
                new FoodImage
                {
                    Id = 1,
                    FoodId = 1,
                    Name = "Wheat Bran – Botanical Colors",
                    ContentUrl = "https://botanicalcolors.com/dir/wp-content/uploads/2018/05/Wheat-Bran.png",
                    ThumbnailUrl = "https://tse2.mm.bing.net/th?id=OIP.lu6mHfalTAtf6QDB_NwergHaHa&pid=Api",
                    HostpageUrl = "https://botanicalcolors.com/shop/mordants/wheat-bran/"
                },
                new FoodImage
                {
                    Id = 2,
                    FoodId = 1,
                    Name = "Wheat Bran Vs. Oat Bran | LIVESTRONG.COM",
                    ContentUrl = "http://img.aws.livestrongcdn.com/ls-article-image-673/cpi.studiod.com/www_livestrong_com/photos.demandstudios.com/getty/article/167/179/97030033_XS.jpg",
                    ThumbnailUrl = "https://tse4.mm.bing.net/th?id=OIP.pm-hVdRM1rDizW-PAHGmBAHaE6&pid=Api",
                    HostpageUrl = "http://www.livestrong.com/article/41815-wheat-bran-vs.-oat-bran/"
                },
                new FoodImage
                {
                    Id = 3,
                    FoodId = 1,
                    Name = "Know the Real Difference Between Wheat Germ and Wheat Bran ...",
                    ContentUrl = "http://media.buzzle.com/media/images-en/photos/agriculture/1200-20995082-wheat-bran.jpg",
                    ThumbnailUrl = "https://tse2.mm.bing.net/th?id=OIP.AImTuYsIkoxz03k5_BV-RwHaE8&pid=Api",
                    HostpageUrl = "http://www.buzzle.com/articles/what-is-the-difference-between-wheat-germ-and-wheat-bran.html"
                },
                new FoodImage
                {
                    Id = 4,
                    FoodId = 1,
                    Name = "Wheat Bran - Bulk Priced Food Shoppe",
                    ContentUrl = "https://bulkpricedfoodshoppe.com/wp-content/uploads/2017/02/product_1_5_156030.jpg",
                    ThumbnailUrl = "https://tse2.mm.bing.net/th?id=OIP.8DDnI7d_uYAOjm4o6CaELwHaG8&pid=Api",
                    HostpageUrl = "https://www.bulkpricedfoodshoppe.com/product/wheat-bran/"
                },
                new FoodImage
                {
                    Id = 5,
                    FoodId = 1,
                    Name = "wheat bran - Hatab",
                    ContentUrl = "https://alhatab.com.sa/wp-content/uploads/2018/02/wheat-bran.jpg",
                    ThumbnailUrl = "https://tse1.mm.bing.net/th?id=OIP.zidwHSvQRWAIvUhbaaWUJwHaFj&pid=Api",
                    HostpageUrl = "https://alhatab.com.sa/nutrition-facts/wheat-bran/"
                }
            );

            context.SaveChanges();
        }

        public static void Destroy(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
    }
}