﻿using GlycemicCatalog.Application.UnitTests.Common;
using GlycemicCatalog.Application.Foods.Commands.DeleteFood;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using GlycemicCatalog.Application.Common.Exceptions;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.Foods.Commands.DeleteFood
{
    public class DeleteFoodCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_GivenValidId_ShouldRemovePersistedTodoItem()
        {
            var command = new DeleteFoodCommand
            {
                Id = 1
            };

            var handler = new DeleteFoodCommand.DeleteFoodCommandHandler(Context);

            await handler.Handle(command, CancellationToken.None);

            var entity = Context.Foods.Find(command.Id);

            entity.ShouldBeNull();
        }

        [Fact]
        public void Handle_GivenInvalidId_ThrowsException()
        {
            var command = new DeleteFoodCommand
            {
                Id = 99
            };

            var handler = new DeleteFoodCommand.DeleteFoodCommandHandler(Context);

            Should.ThrowAsync<NotFoundException>(() =>
                handler.Handle(command, CancellationToken.None));
        }
    }
}
