﻿using GlycemicCatalog.Application.UnitTests.Common;
using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.Foods.Commands.UpdateFood;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.Foods.Commands.UpdateFood
{
    public class UpdateFoodCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_GivenValidId_ShouldUpdatePersistedTodoList()
        {
            var command = new UpdateFoodCommand
            {
                Id = 1,
                Name = "Crispbread. rye"
                // TODO: fill all fields
            };

            var handler = new UpdateFoodCommand.UpdateFoodCommandHandler(Context);

            await handler.Handle(command, CancellationToken.None);

            var entity = Context.Foods.Find(command.Id);

            entity.ShouldNotBeNull();
            entity.Name.ShouldBe(command.Name);
        }

        [Fact]
        public void Handle_GivenInvalidId_ThrowsException()
        {
            var command = new UpdateFoodCommand
            {
                Id = 99,
                Name = "Crispbread. rye"
            };

            var handler = new UpdateFoodCommand.UpdateFoodCommandHandler(Context);

            Should.ThrowAsync<NotFoundException>(() =>
                handler.Handle(command, CancellationToken.None));
        }
    }
}
