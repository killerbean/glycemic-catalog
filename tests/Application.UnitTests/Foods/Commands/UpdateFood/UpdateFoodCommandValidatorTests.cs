﻿using GlycemicCatalog.Application.Foods.Commands.UpdateFood;
using GlycemicCatalog.Application.UnitTests.Common;
using GlycemicCatalog.Domain.Entities;
using Shouldly;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.Foods.Commands.UpdateFood
{
    public class UpdateFoodCommandValidatorTests : CommandTestBase
    {
        [Fact]
        public void IsValid_ShouldBeTrue_WhenListTitleIsUnique()
        {
            var command = new UpdateFoodCommand
            {
                Id = 1,
                Name = "Crispbread. rye"
            };

            var validator = new UpdateFoodCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(true);
        }

        [Fact]
        public void IsValid_ShouldBeFalse_WhenListTitleIsNotUnique()
        {
            Context.Foods.Add(new Food {Name = "Bran. wheat" });
            Context.SaveChanges();

            var command = new UpdateFoodCommand
            {
                Id = 1,
                Name = "Bran. wheat"
            };

            var validator = new UpdateFoodCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(false);
        }
    }
}
