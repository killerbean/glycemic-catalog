﻿using GlycemicCatalog.Application.UnitTests.Common;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using GlycemicCatalog.Application.Foods.Commands.CreateFood;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.Foods.Commands.CreateFood
{
    public class CreateFoodCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_ShouldPersistTodoList()
        {
            var command = new CreateFoodCommand
            {
                Name = "Bran. wheat"
                // TODO: Comlete all fields
            };

            var handler = new CreateFoodCommand.CreateTodoListCommandHandler(Context);

            var result = await handler.Handle(command, CancellationToken.None);

            var entity = Context.Foods.Find(result);

            entity.ShouldNotBeNull();
            entity.Name.ShouldBe(command.Name);
        }        
    }
}
