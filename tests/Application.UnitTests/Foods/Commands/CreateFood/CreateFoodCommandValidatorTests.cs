﻿using GlycemicCatalog.Application.Foods.Commands.CreateFood;
using GlycemicCatalog.Application.UnitTests.Common;
using GlycemicCatalog.Domain.Entities;
using Shouldly;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.Foods.Commands.CreateFood
{
    public class CreateFoodCommandValidatorTests : CommandTestBase
    {
        [Fact]
        public void IsValid_ShouldBeTrue_WhenListTitleIsUnique()
        {
            var command = new CreateFoodCommand
            {
                Name = "Crispbread. rye"
            };

            var validator = new CreateFoodCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(true);
        }

        [Fact]
        public void IsValid_ShouldBeFalse_WhenListTitleIsNotUnique()
        {
            Context.Foods.Add(new Food { Name = "Bran. wheat" });
            Context.SaveChanges();

            var command = new CreateFoodCommand
            {
                Name = "Bran. wheat"
            };

            var validator = new CreateFoodCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(false);
        }
    }
}
