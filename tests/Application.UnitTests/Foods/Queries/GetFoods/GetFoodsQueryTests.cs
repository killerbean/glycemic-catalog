﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GlycemicCatalog.Application.Foods.Queries.GetFoods;
using GlycemicCatalog.Application.UnitTests.Common;
using GlycemicCatalog.Infrastructure.Persistence;
using Shouldly;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.Foods.Queries.GetFoods
{
    [Collection("QueryTests")]
    public class GetFoodsQueryTests
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetFoodsQueryTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _mapper = fixture.Mapper;
        }
        
        [Fact]
        public async Task Handle_ReturnsCorrectVmAndListCount()
        {
            var query = new GetFoodsQuery();

            var handler = new GetFoodsQuery.GetFoodsQueryHandler(_context, _mapper);

            var result = await handler.Handle(query, CancellationToken.None);

            result.ShouldBeOfType<FoodsVm>();
            result.Lists.Count.ShouldBe(1);

            var list = result.Lists.First();

            list.Items.Count.ShouldBe(5);
        }
    }
}