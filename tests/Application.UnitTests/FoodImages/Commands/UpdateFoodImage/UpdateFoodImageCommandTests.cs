﻿using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.FoodImages.Commands.UpdateFoodImage;
using GlycemicCatalog.Application.UnitTests.Common;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.FoodImages.Commands.UpdateFoodImage
{
    public class UpdateFoodImageCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_GivenValidId_ShouldUpdatePersistedFoodImage()
        {
            var command = new UpdateFoodImageCommand
            {
                Id = 1,
                Name = "Wheat Bran – Botanical Colors"
            };

            var handler = new UpdateFoodImageCommand.UpdateFoodImageCommandHandler(Context);

            await handler.Handle(command, CancellationToken.None);

            var entity = Context.FoodImages.Find(command.Id);

            entity.ShouldNotBeNull();
            entity.Name.ShouldBe(command.Name);
        }

        [Fact]
        public void Handle_GivenInvalidId_ThrowsException()
        {
            var command = new UpdateFoodImageCommand
            {
                Id = 99,
                Name = "This item doesn't exist."
            };

            var sut = new UpdateFoodImageCommand.UpdateFoodImageCommandHandler(Context);

            Should.ThrowAsync<NotFoundException>(() => 
                sut.Handle(command, CancellationToken.None));
        }
    }
}