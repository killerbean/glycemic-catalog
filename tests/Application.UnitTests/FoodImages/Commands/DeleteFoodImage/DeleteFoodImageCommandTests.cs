﻿using GlycemicCatalog.Application.Common.Exceptions;
using GlycemicCatalog.Application.FoodImages.Commands.DeleteFoodImage;
using GlycemicCatalog.Application.UnitTests.Common;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.FoodImages.Commands.DeleteFoodImage
{
    public class DeleteFoodImageCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_GivenValidId_ShouldRemovePersistedFoodImage()
        {
            var command = new DeleteFoodImageCommand
            {
                Id = 1
            };

            var handler = new DeleteFoodImageCommand.DeleteTodoItemCommandHandler(Context);

            await handler.Handle(command, CancellationToken.None);

            var entity = Context.FoodImages.Find(command.Id);

            entity.ShouldBeNull();
        }

        [Fact]
        public void Handle_GivenInvalidId_ThrowsException()
        {
            var command = new DeleteFoodImageCommand
            {
                Id = 99
            };

            var handler = new DeleteFoodImageCommand.DeleteTodoItemCommandHandler(Context);

            Should.ThrowAsync<NotFoundException>(() => 
                handler.Handle(command, CancellationToken.None));
        }
    }
}
