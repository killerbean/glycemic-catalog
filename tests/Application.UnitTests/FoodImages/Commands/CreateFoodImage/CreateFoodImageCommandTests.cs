﻿using GlycemicCatalog.Application.FoodImages.Commands.CreateFoodImage;
using GlycemicCatalog.Application.UnitTests.Common;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace GlycemicCatalog.Application.UnitTests.FoodImages.Commands.CreateFoodImage
{
    public class CreateFoodImageCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_ShouldPersistFoodImage()
        {
            var command = new CreateFoodImageCommand
            {
                Name = "Organic Wheat Bran – Kialla Pure Foods"
            };

            var handler = new CreateFoodImageCommand.CreateTodoItemCommandHandler(Context);
            var result = await handler.Handle(command, CancellationToken.None);
            var entity = Context.FoodImages.Find(result);

            entity.ShouldNotBeNull();
            entity.Name.ShouldBe(command.Name);
        }
    }
}